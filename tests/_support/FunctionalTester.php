<?php

use Pages\Google as GooglePage;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class FunctionalTester extends \Codeception\Actor
{
    use _generated\FunctionalTesterActions;

    /**
     * @When I go to google page
     */
    public function iGoGooglePage()
    {
        $this->amOnPage('/');
    }

    /**
     * @Then I search :word
     */
    public function search($word)
    {
        $this->fillField(GooglePage::$searchField, 'QA');
        $this->click(GooglePage::$logo);
        $this->click(GooglePage::$buttonSearch);
        $this->wait(10);
    }


    /**
     * @Then check the result page
     */
    public function checkTheResultPage()
    {
        $this->seeInTitle('QA');
        $this->seeElement('div#ires');
    }

    /**
     * @Then check the result page failed
     */
    public function checkTheResultPageFailed()
    {
        $this->seeInTitle('QA_Test_failed');
        $this->seeElement('div#ires_test_failed');
    }
}
