<?php

namespace stepDefs;

use Pages\Home;
use Pages\Login as LoginPage;
use Pages\SmsPassword as SmsPassword;

class CommonStepDefsHelper extends \Codeception\Module
{
    /**
     * @When I go to the dev home page
     */
    public function iGoHomePage()
    {
        $this->getModule('WebDriver')->amOnPage('/');
    }

    /**
     * @When I login with role :role
     */
    public function loginWithRole($role)
    {
        $I = $this->getModule('WebDriver');
        echo "role2 is: $role";
        $I->amOnPage(LoginPage::$URL);
        switch ($role) {
            case "user":
                $I->fillField(LoginPage::$inputUsername, "alex_user");
                $I->fillField(LoginPage::$inputPassword, "TestPassword1!");
                break;
            case "manager":
                $I->fillField(LoginPage::$inputUsername, "alex_manager");
                $I->fillField(LoginPage::$inputPassword, "TestPassword1!");
                break;
            default:
                $I->fillField(LoginPage::$inputUsername, "a");
                break;
        }
        $I->click(LoginPage::$submitButton);
    }

    /**
     * @When I login with incorrect credentials
     */
    public function incorrectLogin()
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage(LoginPage::$URL);
        $I->fillField(LoginPage::$inputUsername, "incorrect_login");
        $I->fillField(LoginPage::$inputPassword, "IncorrectPassword1!");
        $I->click(LoginPage::$submitButton);
    }

    /**
     * @When I input sms code for non production environment
     */
    public function inputSmsCode()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField(SmsPassword::$inputSmsCode, "123456");
        $I->click(SmsPassword::$submitButton);
    }

    /**
     * @When I input incorrect SMS code
     */
    public function inputIncorrectSmsCode()
    {
        $I = $this->getModule('WebDriver');
        $I->fillField(SmsPassword::$inputSmsCode, "1111111111");
        $I->click(SmsPassword::$submitButton);
    }


    /**
     * @When I logout
     */
    public function logout()
    {
        $I = $this->getModule('WebDriver');
        $I->click(Home::$buttonOpenLogOutDropdown);
        $I->click(Home::$buttonLogOut);
    }


}