<?php

namespace Pages;

class Login
{
    public static $URL = '/login';

    public static $inputUsername = "input#user_name";
    public static $inputPassword = "input[name='password']";
    public static $submitButton = "input.bt_login";
}