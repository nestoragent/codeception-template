<?php

namespace Pages;

class Home
{
    public static $buttonOpenLogOutDropdown = "img[onclick='showLogoutMenu()']";
    public static $buttonLogOut = "div#myDropdown a[href*='logout']";
}