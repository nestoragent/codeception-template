<?php

namespace Pages;

class Google
{
    public static $URL = '/';

    public static $searchField = "input#lst-ib";
    public static $logo = "div#hplogo";
    public static $buttonSearch = "input[jsaction='sf.chk']";
}