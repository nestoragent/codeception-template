<?php

use Page\Google as GooglePage;

class GoogleCest
{

    function _before(FunctionalTester $I)
    {
        $I->amOnPage("/");
    }

    function searchTest(FunctionalTester $I)
    {
        $I->amOnPage(GooglePage::$URL);
        $I->fillField(GooglePage::$searchField, 'QA');
        $I->click(GooglePage::$buttonSearch);
        $I->seeInTitle('QA');
        $I->seeElement('div#ires');
    }
}
