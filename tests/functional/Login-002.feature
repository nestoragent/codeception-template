Feature: Login-002. Login with incorrect credentials 4 times

  @Login-002
  @RT
  Scenario: Login-002. Login with incorrect credentials 4 times
    When I go to the dev home page
    Then I login with incorrect credentials
    Then I login with incorrect credentials
    Then I login with incorrect credentials
    Then I login with incorrect credentials
    When I login with role "user"
    Then I input incorrect SMS code
    Then I input incorrect SMS code
    Then I input incorrect SMS code
    Then I input incorrect SMS code
    Then I input sms code for non production environment
    Then logout