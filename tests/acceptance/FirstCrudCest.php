<?php

use Page\Login as LoginPage;

class FirstCrudCest
{
    function _before(AcceptanceTester $I)
    {
        // will be executed at the beginning of each test
        $I->amOnPage('/');
    }

    function createPage(AcceptanceTester $I)
    {
        $I->wantTo('login to site');
        $I->amOnPage(LoginPage::$URL);
        $I->fillField(LoginPage::$usernameField, 'bill evans');
        $I->fillField(LoginPage::$passwordField, 'debby');
        $I->click(LoginPage::$submitButton);
    }

    function viewPage(AcceptanceTester $I)
    {
        $I->wantTo('wait message');
        $I->amOnPage(LoginPage::$URL);
        $I->fillField(LoginPage::$usernameField, 'Update new login');
        $I->fillField(LoginPage::$passwordField, 'Update new pass');
        $I->click(LoginPage::$submitButton);
        //wait negative
        $I->canSee('Invalid credentials', 'div.negative');

    }

    function updatePage(AcceptanceTester $I)
    {
        $I->wantTo('update text');
        $I->amOnPage(LoginPage::$URL);
        $I->fillField(LoginPage::$usernameField, 'Update new login');
        $I->fillField(LoginPage::$passwordField, 'Update new pass');
        $I->click(LoginPage::$submitButton);

    }

    function deletePage(AcceptanceTester $I)
    {
        $I->wantTo('delete text');
        $I->amOnPage(LoginPage::$URL);
        $I->fillField(LoginPage::$usernameField, '');
        $I->fillField(LoginPage::$passwordField, '');
        $I->click(LoginPage::$submitButton);
        $I->see('', LoginPage::$passwordField);
    }
}
