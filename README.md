### DOCS ###
http://codeception.com

### PRE CONDITION for windows ###

    need to install php, steps:
1. download php from: http://php.net/downloads.php
2. Unpacked to the certain folder
3. add php to the path environment variable
4. set php.ini into php root folder

### TO RUN php simple###
At main folder execute: 
 - php codecept.phar build
 - php codecept.phar run functional

### TO RUN cucumber###
 - php codecept.phar run -g init -v --html
 
### RUN with chrome at windows ###
instruction: http://codeception.com/docs/modules/WebDriver#Selenium

- start standalone server: java -jar "path_to_file/selenium-server-standalone-3.4.0.jar"
- run chromedriver: chromedriver.exe --url-base=/wd/hub 
- run test: php codecept.phar run -g init -v --html

### Exceptions ###
- [Exception] Codeception requires CURL extension installed to make tests run
- [Solution]: Open ini file and update line ";extension=php_curl.dll" to “extension=ext/php_curl.dll"

- [Exception] [Symfony\Component\Debug\Exception\FatalThrowableError]
Call to undefined function Codeception\Module\mb_strcut()
- [Solution] Open ini file and update line ";extension=php_mbstring.dll" to "extension=ext/php_mbstring.dll"